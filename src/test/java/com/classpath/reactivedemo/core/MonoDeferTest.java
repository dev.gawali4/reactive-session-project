package com.classpath.reactivedemo.core;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class MonoDeferTest {
    int initialValue = 10;
    @Test
    public void testMonoDefer(){


        Mono<Integer> just = Mono.just(initialValue);
        Mono<Integer> defer = Mono.defer(() -> Mono.just(initialValue));

//        just.subscribe(val -> System.out.println("Just "+ val));
  //      defer.subscribe(val -> System.out.println("Defer "+ val));

        initialValue = 50;

        just.subscribe(val -> System.out.println("Just "+ val));
        defer.subscribe(val -> System.out.println("Defer "+ val));


    }
}