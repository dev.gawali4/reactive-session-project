package com.classpath.reactivedemo.core;

import com.classpath.reactivedemo.model.User;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FluxTest {

   /* @Test
    public void testFlux(){
        //
        List<User> users = Arrays.asList(
                new User(1, "Naveen", "naveen@gmail.com"),
                new User(2, "Kiran", "kiran@gmail.com"),
                new User(3, "Vinay", "vinay@gmail.com"),
                new User(4, "Harsh", "harsh@gmail.com")
        );

        Flux<User> sourceFlux = Flux.fromIterable(users);

        Flux<String> emailAddresses = sourceFlux.map(user -> user.getEmailAddress());

        List<String> emails = new ArrayList<>();

        //emailAddresses.log().subscribe(email -> System.out.println(email));

        emailAddresses.subscribe(emails::add);


        emails.stream().forEach(e -> System.out.println(e));
    }*/

    @Test
    public void fluxWithError(){
        //
        List<User> users = Arrays.asList(
                User.builder().id(1).name("Naveen").emailAddress("naveen@gmail.com").build(),
                User.builder().id(2).name("Kiran").emailAddress("kiran@gmail.com").build(),
                User.builder().id(3).name("Vinay").emailAddress("vinay@gmail.com").build(),
                User.builder().id(4).name("Harish").emailAddress("harish@gmail.com").build()
        );

        Flux<User> sourceFlux = Flux.fromIterable(users);
        sourceFlux = sourceFlux.concatWith(Flux.error(new RuntimeException("Exception thrown")));
       // sourceFlux = sourceFlux.concatWith(Flux.just(new User(4 ,"Vishal", "vishal@gmail.com")));

       // sourceFlux.log().subscribe(user -> System.out.println(user.getEmailAddress()));

        sourceFlux.subscribe(new Subscriber<User>() {
            @Override
            public void onSubscribe(Subscription s) {
               s.request(10);
            }

            @Override
            public void onNext(User user) {
                System.out.println("On next :: "+ user);
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("On error:::  "+ t.getMessage());
            }

            @Override
            public void onComplete() {
                System.out.println("On complete ");
            }
        });

        StepVerifier.create(sourceFlux.log())
                .expectSubscription()
                .expectNext(
                        User.builder().id(1).name("Naveen").emailAddress("naveen@gmail.com").build(),
                        User.builder().id(2).name("Kiran").emailAddress("kiran@gmail.com").build(),
                        User.builder().id(3).name("Vinay").emailAddress("vinay@gmail.com").build(),
                        User.builder().id(4).name("Harish").emailAddress("harish@gmail.com").build()
                )
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    public void testMonoWithException(){
        Mono<String> values = Mono.just("SUNDAY")
                                //.then(Mono.error(new IllegalArgumentException("Invalid data")));
                                .then(Mono.empty());

        values.subscribe(System.out::println, (e) -> System.out.println(e.getMessage()), ()-> System.out.println("Flux is completed state"));
    }

    @Test
    public void delayMono() throws InterruptedException {
        Mono<String> data = Mono.just("data")
                .delayElement(Duration.ofSeconds(2));
        System.out.println("Start time :: "+ System.currentTimeMillis());
        data.subscribe(value -> System.out.println(value));
        System.out.println("End time :: "+ System.currentTimeMillis());
        Thread.sleep(5000);
    }

    @Test
    public void delayFlux() throws InterruptedException {
        Flux.just("one", "two", "three", "four", "one", "two", "three", "four")
                .delayElements(Duration.ofSeconds(1))
                .log()
                .subscribe(System.out::println);
        Thread.sleep(10000);
    }

    @Test
    public void testFluxFilter(){
        Flux
                .just(11, 22, 33, 10, 17, 44, 55, 66, 9)
                .filter(age -> age > 18)
                .log()
                .map(age -> age * 365)
                .subscribe(values -> System.out.println("Users with age greater than 18 days old::  "+ values));
    }

    //flatMap

    @Test
    public void testFlatMapOperator() {
        List<User> users = Arrays.asList(
                User.builder().id(1).name("Naveen").emailAddress("naveen@gmail.com").phoneNumbers(Arrays.asList("1111111", "22222222", "33333333")).build(),
                User.builder().id(2).name("Kiran").emailAddress("kiran@gmail.com").phoneNumbers(Arrays.asList("44444444", "55555555", "66666666")).build(),
                User.builder().id(3).name("Vinay").emailAddress("vinay@gmail.com").phoneNumbers(Arrays.asList("77777777", "88888888", "99999999")).build(),
                User.builder().id(4).name("Harish").emailAddress("harish@gmail.com").phoneNumbers(Arrays.asList("12312312", "7878787878", "989899989898")).build()
        );

        Flux.fromIterable(users)
                .log()
                .flatMap(user -> Flux.fromIterable(user.getPhoneNumbers()))
                .subscribe(phonenumbers -> System.out.println(phonenumbers));
    }

    @Test
    public void concatMapTest(){

        Flux<Integer> numbers1 = Flux.range(11, 4);
        Flux<Integer> numbers2 = Flux.range(22, 4);

       // numbers1.concatWith(numbers2)
         //       .subscribe(System.out::println);

        Flux.concat(numbers1, numbers2).log().subscribe(data -> System.out.println(data));
    }
    @Test
    public void mergeMapTest() throws InterruptedException {

        Flux<Integer> numbers1 = Flux.range(11, 4);
        Flux<Integer> numbers2 = Flux.range(22, 4);

       // numbers1.concatWith(numbers2)
         //       .subscribe(System.out::println);

        Flux.concat(numbers1.delayElements(Duration.ofSeconds(2)), numbers2.delayElements(Duration.ofSeconds(3)))
                .subscribe(data -> System.out.println(data));
          Thread.sleep(10_000);
    }

    @Test
    public void testZipOperator(){
        Flux<Integer> numbers1 = Flux.range(11, 5);
        Flux<Integer> numbers2 = Flux.range(22, 5);
        Flux<String> names = Flux.fromIterable(Arrays.asList("Anand", "Avinash", "Anil", "Bhaskar", "Bhanu", "Bhavya", "Bharath"));

        Flux
                .zip(numbers1, numbers2, names)
                .subscribe(data -> System.out.println(data) , (e) -> System.out.println(e.getMessage()), () -> System.out.println("Zip operator completed successfully ::"));

    }

    @Test
    public void zipCompute(){
        Flux<Integer> numbers1 = Flux.range(11, 5);
        Flux<Integer> numbers2 = Flux.range(22, 5);

        Flux.zip(numbers1, numbers2, (a, b) -> a * b)
                .subscribe(System.out::println);

    }

    @Test
    public void testSwitchIfEmpty(){
        Flux<Integer> emptyFlux = Flux.range(1, 4);
        Flux<Integer> dataFlux = Flux.range(10, 10);

        emptyFlux.switchIfEmpty(dataFlux).subscribe(System.out::println);
    }

    @Test
    public void testSwitch(){
        Flux<Integer> emptyFlux = Flux.range(1, 4);
        Flux<Integer> dataFlux = Flux.range(10, 10);

        emptyFlux.switchMap(integer -> {
            System.out.println("Value of I is "+ integer);
            if (integer % 2 == 0) {
                return dataFlux;
            }else {
                return emptyFlux;
            }
        }).subscribe(System.out::println);
    }

    @Test
    public void testTake() throws InterruptedException {
        Flux<Integer> numbers = Flux.range(100, 200);
        numbers
                .take(5)
                .map(val -> val  * Math.ceil(Math.random() * 100))
                .filter(val -> val % 2 == 0)
                .delayElements(Duration.ofSeconds(2))
                .blockLast();
               numbers.subscribe(val -> System.out.println(val));
    }
}