package com.classpath.reactivedemo.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import reactor.util.retry.Retry;

import java.time.Duration;

@Slf4j
public class ExceptionHandling {

    @Test
    public void fluxErrorHandling(){
        Flux<Integer> numbers =  Flux.just(11, 22, 33, 44)
                                     .concatWith(Flux.error(new RuntimeException("Failed to fetch the number")))
                                     .concatWith(Flux.just(55))
                                     .onErrorResume((e) -> {
                                         log.error("Exception occurred while fetching the data :: "+ e.getMessage());
                                         return Flux.error(new IllegalArgumentException(" Invalid data"));
                                     });

        StepVerifier
                .create(numbers.log())
                .expectSubscription()
                .expectNext(11,22,33,44)
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    public void fluxErrorHandlingVerifyComplete(){
        Flux<Integer> numbers =  Flux.just(11, 22, 33, 44)
                                     .concatWith(Flux.error(new RuntimeException("Failed to fetch the number")))
                                     .concatWith(Flux.just(55))
                                     .onErrorResume((e) -> {
                                         log.error("Exception occurred while fetching the data :: "+ e.getMessage());
                                         return Flux.just(66);
                                     });

        StepVerifier
                .create(numbers.log())
                .expectSubscription()
                .expectNext(11,22,33,44)
                .expectNext(66)
                //.expectError(RuntimeException.class)
                .verifyComplete();
    }


    @Test
    public void fluxErrorHandlingWithMap(){
        Flux<Integer> numbers =  Flux.just(11, 22, 33, 44)
                                     .concatWith(Flux.error(new RuntimeException("Failed to fetch the number")))
                                     .concatWith(Flux.just(55))
                                     .onErrorMap((e) -> {
                                         log.error("Exception occurred while fetching the data :: "+ e.getMessage());
                                         return new IllegalArgumentException("Rethrow IllegalArgumentException");
                                     });

        StepVerifier
                .create(numbers.log())
                .expectSubscription()
                .expectNext(11,22,33,44)
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    public void fluxErrorHandlingWithOnError(){
        Flux<Integer> numbers =  Flux.just(11, 22, 33, 44)
                                     .concatWith(Flux.error(new RuntimeException("Failed to fetch the number")))
                                     .concatWith(Flux.just(55))
                                     .doOnError(exception -> Flux.just(66));

        StepVerifier
                .create(numbers.log())
                .expectSubscription()
                .expectNext(11,22,33,44)
                //.expectError(RuntimeException.class)
                .verifyComplete();

    }

    @Test
    public void fluxErrorHandlingWithRetry(){
        Flux<Integer> numbers =  Flux.just(11, 22, 33, 44)
                                     .concatWith(Flux.error(new RuntimeException("Failed to fetch the number")))
                                     .concatWith(Flux.just(55))
                                     .retryWhen(Retry.fixedDelay(2, Duration.ofSeconds(4)));

        StepVerifier
                .create(numbers.log())
                .expectSubscription()
                .expectNext(11,22,33,44)
                .expectNext(11,22,33,44)
                .expectNext(11,22,33,44)
                .expectError(RuntimeException.class)
                .verify();

    }
    @Test
    public void fluxErrorHandlingWithRetryBackOff(){
        Flux<Integer> numbers =  Flux.just(11, 22, 33, 44)
                                     .concatWith(Flux.error(new RuntimeException("Failed to fetch the number")))
                                     .concatWith(Flux.just(55))
                                     .retryWhen(Retry.backoff(2, Duration.ofSeconds(2)));

        StepVerifier
                .create(numbers.log())
                .expectSubscription()
                .expectNext(11,22,33,44)
                .expectNext(11,22,33,44)
                .expectNext(11,22,33,44)
                .expectError(RuntimeException.class)
                .verify();

    }
}