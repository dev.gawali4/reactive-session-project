package com.classpath.reactivedemo.core;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.function.Consumer;

@Component
public class PublisherDemo implements CommandLineRunner {
    static int counter;
    @Override
    public void run(String... args) throws Exception {
        System.out.println(" Inside the run method of Publisher demo ....");
        //monoDemo();
        //fluxDemo();
        //pubisherDemo();
        //mapOperator();
        fluxWithArray();

    }

    private void fluxWithArray() {
        Integer[] array = new Integer[]{11,22,33,44};
        Flux<Integer> flux = Flux.fromArray(array);
        Flux<Integer> fluxWithIterable = Flux.fromIterable(Arrays.asList(array));

        fluxWithIterable.
                log()
                .subscribe(elem -> System.out.println(elem));


    }

    private void mapOperator() {
        Flux<Integer> integerFlux = Flux.just(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        integerFlux
                .log()
                .map(value -> value * 10)
                .subscribe(value -> System.out.println(value));
    }

    private void pubisherDemo() {
        Subscriber<?> subscriber = new Subscriber<String>() {
            Subscription subscription;

            @Override
            public void onSubscribe(Subscription subscription) {
                this.subscription = subscription;
                subscription.request(4);
                ++counter;
            }

            @Override
            public void onNext(String s) {
                    this.subscription.request(2);
                    System.out.println(s);
            }

            @Override
            public void onError(Throwable t) {
                System.out.println(" There is an error");
                System.out.println(t.getMessage());
            }

            @Override
            public void onComplete() {
                System.out.println(" Completion of all the messages");
                this.subscription.cancel();
            }
        };
        //Publisher
        Flux<String> publisher = Flux.just("one", "two", "three", "four", "five", "six", "seven", "nine", "ten");
        publisher.subscribe(data -> System.out.println(data));
    }

    private void fluxDemo() {

        Consumer<String> stringConsumer = (data) -> System.out.println(data);
        //Publisher
        Flux<String> flux = Flux.just("one", "two", "three", "four");
        flux.subscribe(stringConsumer);
        flux.subscribe(data -> System.out.println(data.toUpperCase()));
    }

    private void monoDemo() {
        // Publisher
        Mono<String> one = Mono.just("one");
        //System.out.println(one);
        one.subscribe(System.out::println);
    }
}